library IEEE;
use IEEE.std_logic_1164.ALL;

entity typebuilder is
  port (                
    clk    :in  std_logic;                    
    reset  :in  std_logic;
    blocktype   :out std_logic_vector (2 downto 0)                     
  );
end entity;

