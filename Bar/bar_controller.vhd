library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.numeric_std.all;

entity bar_controller is
	port( clk          :in     std_logic;
	      enable       :in     std_logic;
		    reset        :in     std_logic;
        output_y0    :out    unsigned(7 downto 0);
        output_y1    :out    unsigned(7 downto 0);
        output_y2    :out    unsigned(7 downto 0);
        output_y3    :out    unsigned(7 downto 0);
        output_type0 :out    std_logic_vector(2 downto 0);
        output_type1 :out    std_logic_vector(2 downto 0);
        output_type2 :out    std_logic_vector(2 downto 0);
        output_type3 :out    std_logic_vector(2 downto 0)
	);
end bar_controller;


