library IEEE;
use IEEE.std_logic_1164.ALL;

entity vga_core is
	PORT(clk, reset 	:in 	std_logic;
    	x_b1     	:in	std_logic_vector(6 downto 0);
    	y_b1     	:in	std_logic_vector(7 downto 0);
    	x_b2     	:in	std_logic_vector(6 downto 0);
    	y_b2     	:in	std_logic_vector(7 downto 0);
    	y_o1     	:in	std_logic_vector(7 downto 0);
    	t_o1     	:in	std_logic_vector(2 downto 0);
   	y_o2     	:in	std_logic_vector(7 downto 0);
    	t_o2     	:in	std_logic_vector(2 downto 0);
   	y_o3     	:in	std_logic_vector(7 downto 0);
    	t_o3     	:in	std_logic_vector(2 downto 0);
   	y_o4     	:in	std_logic_vector(7 downto 0);
    	t_o4     	:in	std_logic_vector(2 downto 0);
	y2_o1     :in    std_logic_vector(7 downto 0);
        y2_o2     :in    std_logic_vector(7 downto 0);
        y2_o3     :in    std_logic_vector(7 downto 0);
        y2_o4     :in    std_logic_vector(7 downto 0);
        y2_b1     :in    std_logic_vector(7 downto 0);
        y2_b2     :in    std_logic_vector(7 downto 0);
        x2_b1     :in    std_logic_vector(6 downto 0);
        x2_b2     :in    std_logic_vector(6 downto 0);
	rom_data	 :in    std_logic_vector(7 downto 0);
	rom_address	  :out	 std_logic_vector(4 downto 0);
	row_address    :in 	std_logic_vector(6 downto 0);
        col_address 	  :in 	std_logic_vector(7 downto 0);
	color:out std_logic_vector(1 downto 0);    
        rom_mux_output : out std_logic);
end vga_core;

