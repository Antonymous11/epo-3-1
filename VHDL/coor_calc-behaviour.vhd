library IEEE;
use IEEE.std_logic_1164.ALL;
USE  IEEE.STD_LOGIC_UNSIGNED.all;

architecture behaviour of coor_calc is
begin
x2_b1 <= x_b1 + "0001000"; -- meest linker x van bal1
x2_b2 <= x_b2 + "0001000";
y2_b1 <= y_b1 + "00001000"; -- meest lage y van bal1
y2_b2 <= y_b2 + "00001000";
y2_o1  <= y_o1 + "00001000";
y2_o2  <= y_o2 + "00001000";
y2_o3  <= y_o3 + "00001000";
y2_o4  <= y_o4 + "00001000";
end behaviour;

