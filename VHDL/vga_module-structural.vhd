library ieee;
use ieee.std_logic_1164.all;

architecture structural of vga_module is
component vga_hub is
	PORT(clk, reset 	:in 	std_logic;
    	x_b1     	:in	std_logic_vector(6 downto 0);
    	y_b1     	:in	std_logic_vector(7 downto 0);
    	x_b2     	:in	std_logic_vector(6 downto 0);
    	y_b2     	:in	std_logic_vector(7 downto 0);
    	y_o1     	:in	std_logic_vector(7 downto 0);
    	t_o1     	:in	std_logic_vector(2 downto 0);
   	 y_o2     	:in	std_logic_vector(7 downto 0);
    	t_o2     	:in	std_logic_vector(2 downto 0);
   	 y_o3     	:in	std_logic_vector(7 downto 0);
    	t_o3     	:in	std_logic_vector(2 downto 0);
   	 y_o4     	:in	std_logic_vector(7 downto 0);
    	t_o4     	:in	std_logic_vector(2 downto 0);
	 row 		:in 	std_logic_vector(8 downto 0);
         column 	:in 	std_logic_vector(7 downto 0);
         vga_red, vga_green, vga_blue : out std_logic);
end component;


component vga_sync is
 	port(	clk, reset : in std_logic;
 		hsync, vsync : out std_logic;
 		row : out std_logic_vector(8 downto 0);
 		column : out std_logic_vector(7 downto 0));
end component;
		
signal 	row: std_logic_vector(8 downto 0);
signal 	column : std_logic_vector(7 downto 0); 

begin

a1 : vga_sync port map (clk, reset, hsync, vsync, row, column);

a2 : vga_hub port map 	(clk, 
			reset, 
			x_b1, 
			y_b1, 
			x_b2, 
			y_b2, 
			y_o1, 
			t_o1, 
			y_o2, 
			t_o2, 
			y_o3, 
			t_o3, 
			y_o4, 
			t_o4,
			row,
			column,
			red,green,blue);


end architecture;

