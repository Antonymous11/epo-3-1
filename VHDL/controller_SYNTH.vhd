
library ieee,CellsLib;

use ieee.std_logic_1164.all;
use CellsLib.CellsLib_DECL_PACK.all;

architecture synthesised of controller is

   component iv110
      port( A : in std_logic;  Y : out std_logic);
   end component;
   
   component no210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component na310
      port( A, B, C : in std_logic;  Y : out std_logic);
   end component;
   
   component ex210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component na210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component mu111
      port( A, B, S : in std_logic;  Y : out std_logic);
   end component;
   
   component no310
      port( A, B, C : in std_logic;  Y : out std_logic);
   end component;
   
   component dfr11
      port( D, R, CK : in std_logic;  Q : out std_logic);
   end component;
   
   component dfn10
      port( D, CK : in std_logic;  Q : out std_logic);
   end component;
   
   signal counter_2_port, counter_1_port, counter_0_port, n81, n82, n83, n84, 
      n85, n86, n87, n88, n89, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99
      , n100, n101, n102, n103, n104, n105, n106, n107, n108, n109, n110, n111,
      n112, n113, n114, n115, n116, n117, n118, n119, n120, n121, n122, n123, 
      n124, n125, n126, n127, n128, n129, n130, n131, n132, n133, n134, n135, 
      n136, n137, n138, n139, n140, n141, n142, n143, n144, n145, n146, n147, 
      n148, n149, n150 : std_logic;

begin
   
   counter_reg_0_inst : dfn10 port map( D => n89, CK => counter_clk, Q => 
                           counter_0_port);
   counter_reg_1_inst : dfn10 port map( D => n88, CK => counter_clk, Q => 
                           counter_1_port);
   counter_reg_2_inst : dfn10 port map( D => n87, CK => counter_clk, Q => 
                           counter_2_port);
   quadrant_reg_0_inst : dfr11 port map( D => n86, R => reset, CK => 
                           counter_clk, Q => quadrant(0));
   step_reg_2_inst : dfr11 port map( D => n85, R => reset, CK => counter_clk, Q
                           => step(2));
   step_reg_0_inst : dfr11 port map( D => n84, R => reset, CK => counter_clk, Q
                           => step(0));
   step_reg_1_inst : dfr11 port map( D => n83, R => reset, CK => counter_clk, Q
                           => step(1));
   step_reg_3_inst : dfr11 port map( D => n82, R => reset, CK => counter_clk, Q
                           => step(3));
   quadrant_reg_1_inst : dfr11 port map( D => n81, R => reset, CK => 
                           counter_clk, Q => quadrant(1));
   U92 : mu111 port map( A => n90, B => reset, S => counter_0_port, Y => n89);
   U93 : na210 port map( A => n91, B => n92, Y => n88);
   U94 : na210 port map( A => n93, B => n90, Y => n92);
   U95 : na210 port map( A => counter_1_port, B => n94, Y => n91);
   U96 : iv110 port map( A => n95, Y => n94);
   U97 : iv110 port map( A => n96, Y => n87);
   U98 : mu111 port map( A => n97, B => n95, S => counter_2_port, Y => n96);
   U99 : no210 port map( A => reset, B => n98, Y => n95);
   U100 : no210 port map( A => n99, B => counter_0_port, Y => n98);
   U101 : na310 port map( A => counter_1_port, B => counter_0_port, C => n90, Y
                           => n97);
   U102 : no210 port map( A => n99, B => reset, Y => n90);
   U103 : iv110 port map( A => n100, Y => n86);
   U104 : ex210 port map( A => quadrant(0), B => n101, Y => n100);
   U105 : na310 port map( A => step(3), B => n102, C => n103, Y => n101);
   U106 : no210 port map( A => step(2), B => step(1), Y => n103);
   U107 : na310 port map( A => n104, B => n105, C => n106, Y => n85);
   U108 : mu111 port map( A => n107, B => n108, S => step(2), Y => n106);
   U109 : no210 port map( A => n109, B => n110, Y => n108);
   U110 : mu111 port map( A => n111, B => n112, S => step(1), Y => n110);
   U111 : na310 port map( A => step(1), B => step(0), C => n111, Y => n107);
   U112 : na210 port map( A => n113, B => n93, Y => n104);
   U113 : mu111 port map( A => step(0), B => n114, S => n115, Y => n84);
   U114 : na210 port map( A => n102, B => n93, Y => n114);
   U115 : na210 port map( A => n116, B => n105, Y => n83);
   U116 : na310 port map( A => n117, B => n118, C => n115, Y => n105);
   U117 : iv110 port map( A => n119, Y => n116);
   U118 : mu111 port map( A => n120, B => n109, S => step(1), Y => n119);
   U119 : na210 port map( A => n121, B => n115, Y => n109);
   U120 : iv110 port map( A => n122, Y => n121);
   U121 : mu111 port map( A => n111, B => n112, S => step(0), Y => n122);
   U122 : no210 port map( A => n123, B => n117, Y => n112);
   U123 : no210 port map( A => n117, B => n124, Y => n111);
   U124 : no210 port map( A => n117, B => n125, Y => n120);
   U125 : mu111 port map( A => n123, B => n124, S => step(0), Y => n125);
   U126 : mu111 port map( A => n126, B => n127, S => step(3), Y => n82);
   U127 : na210 port map( A => n115, B => n128, Y => n127);
   U128 : na310 port map( A => n129, B => n130, C => n93, Y => n128);
   U129 : na210 port map( A => n131, B => n123, Y => n130);
   U130 : iv110 port map( A => n132, Y => n131);
   U131 : na210 port map( A => n124, B => n133, Y => n129);
   U132 : iv110 port map( A => n134, Y => n133);
   U133 : no210 port map( A => n99, B => n135, Y => n115);
   U134 : no210 port map( A => n136, B => n93, Y => n135);
   U135 : iv110 port map( A => n137, Y => n136);
   U136 : na310 port map( A => step(3), B => step(0), C => n138, Y => n137);
   U137 : ex210 port map( A => n139, B => step(1), Y => n138);
   U138 : no210 port map( A => n140, B => n117, Y => n126);
   U139 : iv110 port map( A => n93, Y => n117);
   U140 : no310 port map( A => counter_1_port, B => counter_2_port, C => n141, 
                           Y => n93);
   U141 : iv110 port map( A => counter_0_port, Y => n141);
   U142 : no210 port map( A => n142, B => n113, Y => n140);
   U143 : no210 port map( A => n134, B => n123, Y => n113);
   U144 : mu111 port map( A => n143, B => n144, S => left, Y => n123);
   U145 : na210 port map( A => right, B => n144, Y => n143);
   U146 : na310 port map( A => n118, B => n139, C => n145, Y => n134);
   U147 : no210 port map( A => n124, B => n132, Y => n142);
   U148 : na310 port map( A => step(2), B => step(0), C => step(1), Y => n132);
   U149 : mu111 port map( A => n146, B => n147, S => left, Y => n124);
   U150 : na210 port map( A => right, B => n147, Y => n146);
   U151 : iv110 port map( A => n144, Y => n147);
   U152 : ex210 port map( A => quadrant(0), B => n148, Y => n144);
   U153 : ex210 port map( A => n148, B => n149, Y => n81);
   U154 : na310 port map( A => step(3), B => n102, C => n150, Y => n149);
   U155 : no210 port map( A => n139, B => n118, Y => n150);
   U156 : iv110 port map( A => step(1), Y => n118);
   U157 : iv110 port map( A => step(2), Y => n139);
   U158 : no210 port map( A => n145, B => n99, Y => n102);
   U159 : no210 port map( A => left, B => right, Y => n99);
   U160 : iv110 port map( A => step(0), Y => n145);
   U161 : iv110 port map( A => quadrant(1), Y => n148);

end synthesised;



