
library ieee,CellsLib;

use ieee.std_logic_1164.all;
use CellsLib.CellsLib_DECL_PACK.all;

architecture synthesised of counter is

   component iv110
      port( A : in std_logic;  Y : out std_logic);
   end component;
   
   component ex210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component no210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component na210
      port( A, B : in std_logic;  Y : out std_logic);
   end component;
   
   component na310
      port( A, B, C : in std_logic;  Y : out std_logic);
   end component;
   
   component no310
      port( A, B, C : in std_logic;  Y : out std_logic);
   end component;
   
   component dfn10
      port( D, CK : in std_logic;  Q : out std_logic);
   end component;
   
   signal count_out_port, count_15_port, count_14_port, count_13_port, 
      count_12_port, count_11_port, count_10_port, count_9_port, count_8_port, 
      count_7_port, count_6_port, count_5_port, count_4_port, count_3_port, 
      count_2_port, count_1_port, count_0_port, N20, N21, N22, N23, N24, N25, 
      N26, N27, N28, N29, N30, N31, N32, N33, N34, N35, N36, n40, n41, n42, n43
      , n44, n45, n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, 
      n58, n59, n60, n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72
      , n73, n74, n75, n76, n77, n78, n79, n80, n81, n82, n83, n84, n85, n86, 
      n87, n88, n89, n90, n91, n92, n93, n94, n95, n96, n97, add_29_n31, 
      add_29_n30, add_29_n29, add_29_n28, add_29_n27, add_29_n26, add_29_n25, 
      add_29_n24, add_29_n23, add_29_n22, add_29_n21, add_29_n20, add_29_n19, 
      add_29_n18, add_29_n17, add_29_n16, add_29_n15, add_29_n14, add_29_n13, 
      add_29_n12, add_29_n11, add_29_n10, add_29_n9, add_29_n8, add_29_n7, 
      add_29_n6 : std_logic;

begin
   count_out <= count_out_port;
   
   count_reg_0_inst : dfn10 port map( D => n56, CK => clk, Q => count_0_port);
   count_reg_16_inst : dfn10 port map( D => n55, CK => clk, Q => count_out_port
                           );
   count_reg_1_inst : dfn10 port map( D => n40, CK => clk, Q => count_1_port);
   count_reg_2_inst : dfn10 port map( D => n41, CK => clk, Q => count_2_port);
   count_reg_3_inst : dfn10 port map( D => n42, CK => clk, Q => count_3_port);
   count_reg_4_inst : dfn10 port map( D => n43, CK => clk, Q => count_4_port);
   count_reg_5_inst : dfn10 port map( D => n44, CK => clk, Q => count_5_port);
   count_reg_6_inst : dfn10 port map( D => n45, CK => clk, Q => count_6_port);
   count_reg_7_inst : dfn10 port map( D => n46, CK => clk, Q => count_7_port);
   count_reg_8_inst : dfn10 port map( D => n47, CK => clk, Q => count_8_port);
   count_reg_9_inst : dfn10 port map( D => n48, CK => clk, Q => count_9_port);
   count_reg_10_inst : dfn10 port map( D => n49, CK => clk, Q => count_10_port)
                           ;
   count_reg_11_inst : dfn10 port map( D => n50, CK => clk, Q => count_11_port)
                           ;
   count_reg_12_inst : dfn10 port map( D => n51, CK => clk, Q => count_12_port)
                           ;
   count_reg_13_inst : dfn10 port map( D => n52, CK => clk, Q => count_13_port)
                           ;
   count_reg_14_inst : dfn10 port map( D => n53, CK => clk, Q => count_14_port)
                           ;
   count_reg_15_inst : dfn10 port map( D => n54, CK => clk, Q => count_15_port)
                           ;
   U59 : no310 port map( A => n58, B => reset, C => n95, Y => n57);
   U60 : no310 port map( A => n95, B => reset, C => n96, Y => n58);
   U61 : na210 port map( A => n59, B => n60, Y => n56);
   U62 : na210 port map( A => N20, B => n61, Y => n60);
   U63 : na210 port map( A => count_0_port, B => n58, Y => n59);
   U64 : na210 port map( A => n63, B => n64, Y => n55);
   U65 : na210 port map( A => N36, B => n57, Y => n64);
   U66 : na210 port map( A => n62, B => count_out_port, Y => n63);
   U67 : na210 port map( A => n65, B => n66, Y => n54);
   U68 : na210 port map( A => N35, B => n61, Y => n66);
   U69 : na210 port map( A => n58, B => count_15_port, Y => n65);
   U70 : na210 port map( A => n67, B => n68, Y => n53);
   U71 : na210 port map( A => N34, B => n57, Y => n68);
   U72 : na210 port map( A => count_14_port, B => n62, Y => n67);
   U73 : na210 port map( A => n69, B => n70, Y => n52);
   U74 : na210 port map( A => N33, B => n61, Y => n70);
   U75 : na210 port map( A => count_13_port, B => n58, Y => n69);
   U76 : na210 port map( A => n71, B => n72, Y => n51);
   U77 : na210 port map( A => N32, B => n57, Y => n72);
   U78 : na210 port map( A => n62, B => count_12_port, Y => n71);
   U79 : na210 port map( A => n73, B => n74, Y => n50);
   U80 : na210 port map( A => N31, B => n61, Y => n74);
   U81 : na210 port map( A => count_11_port, B => n58, Y => n73);
   U82 : na210 port map( A => n75, B => n76, Y => n49);
   U83 : na210 port map( A => N30, B => n57, Y => n76);
   U84 : na210 port map( A => count_10_port, B => n62, Y => n75);
   U85 : na210 port map( A => n77, B => n78, Y => n48);
   U86 : na210 port map( A => N29, B => n61, Y => n78);
   U87 : na210 port map( A => count_9_port, B => n58, Y => n77);
   U88 : na210 port map( A => n79, B => n80, Y => n47);
   U89 : na210 port map( A => N28, B => n57, Y => n80);
   U90 : na210 port map( A => count_8_port, B => n62, Y => n79);
   U91 : na210 port map( A => n81, B => n82, Y => n46);
   U92 : na210 port map( A => N27, B => n61, Y => n82);
   U93 : na210 port map( A => count_7_port, B => n58, Y => n81);
   U94 : na210 port map( A => n83, B => n84, Y => n45);
   U95 : na210 port map( A => N26, B => n57, Y => n84);
   U96 : na210 port map( A => count_6_port, B => n62, Y => n83);
   U97 : na210 port map( A => n85, B => n86, Y => n44);
   U98 : na210 port map( A => N25, B => n61, Y => n86);
   U99 : na210 port map( A => count_5_port, B => n58, Y => n85);
   U100 : na210 port map( A => n87, B => n88, Y => n43);
   U101 : na210 port map( A => N24, B => n57, Y => n88);
   U102 : na210 port map( A => count_4_port, B => n62, Y => n87);
   U103 : na210 port map( A => n89, B => n90, Y => n42);
   U104 : na210 port map( A => N23, B => n61, Y => n90);
   U105 : na210 port map( A => count_3_port, B => n58, Y => n89);
   U106 : na210 port map( A => n91, B => n92, Y => n41);
   U107 : na210 port map( A => N22, B => n57, Y => n92);
   U108 : na210 port map( A => count_2_port, B => n62, Y => n91);
   U109 : na210 port map( A => n93, B => n94, Y => n40);
   U110 : na210 port map( A => N21, B => n61, Y => n94);
   U111 : no310 port map( A => n58, B => reset, C => n95, Y => n61);
   U112 : na210 port map( A => count_1_port, B => n62, Y => n93);
   U113 : no310 port map( A => n95, B => reset, C => n96, Y => n62);
   U114 : iv110 port map( A => rip, Y => n96);
   U115 : iv110 port map( A => n97, Y => n95);
   U116 : na310 port map( A => count_15_port, B => count_12_port, C => 
                           count_out_port, Y => n97);
   add_29_U43 : na310 port map( A => count_1_port, B => count_0_port, C => 
                           count_2_port, Y => add_29_n20);
   add_29_U42 : no310 port map( A => add_29_n6, B => add_29_n20, C => add_29_n7
                           , Y => add_29_n18);
   add_29_U41 : na310 port map( A => count_5_port, B => add_29_n18, C => 
                           count_6_port, Y => add_29_n15);
   add_29_U40 : no310 port map( A => add_29_n8, B => add_29_n15, C => add_29_n9
                           , Y => add_29_n13);
   add_29_U39 : na210 port map( A => add_29_n13, B => count_9_port, Y => 
                           add_29_n31);
   add_29_U38 : ex210 port map( A => count_10_port, B => add_29_n31, Y => 
                           add_29_n30);
   add_29_U37 : na310 port map( A => add_29_n13, B => count_9_port, C => 
                           count_10_port, Y => add_29_n28);
   add_29_U36 : ex210 port map( A => add_29_n10, B => add_29_n28, Y => N31);
   add_29_U35 : no210 port map( A => add_29_n28, B => add_29_n10, Y => 
                           add_29_n29);
   add_29_U34 : ex210 port map( A => count_12_port, B => add_29_n29, Y => N32);
   add_29_U33 : no310 port map( A => add_29_n10, B => add_29_n28, C => 
                           add_29_n11, Y => add_29_n25);
   add_29_U32 : ex210 port map( A => count_13_port, B => add_29_n25, Y => N33);
   add_29_U31 : na210 port map( A => count_13_port, B => add_29_n25, Y => 
                           add_29_n27);
   add_29_U30 : ex210 port map( A => count_14_port, B => add_29_n27, Y => 
                           add_29_n26);
   add_29_U29 : na310 port map( A => count_13_port, B => add_29_n25, C => 
                           count_14_port, Y => add_29_n24);
   add_29_U28 : ex210 port map( A => add_29_n12, B => add_29_n24, Y => N35);
   add_29_U27 : no210 port map( A => add_29_n24, B => add_29_n12, Y => 
                           add_29_n23);
   add_29_U26 : ex210 port map( A => count_out_port, B => add_29_n23, Y => N36)
                           ;
   add_29_U25 : ex210 port map( A => count_1_port, B => count_0_port, Y => N21)
                           ;
   add_29_U24 : na210 port map( A => count_1_port, B => count_0_port, Y => 
                           add_29_n22);
   add_29_U23 : ex210 port map( A => count_2_port, B => add_29_n22, Y => 
                           add_29_n21);
   add_29_U22 : ex210 port map( A => add_29_n6, B => add_29_n20, Y => N23);
   add_29_U21 : no210 port map( A => add_29_n20, B => add_29_n6, Y => 
                           add_29_n19);
   add_29_U20 : ex210 port map( A => count_4_port, B => add_29_n19, Y => N24);
   add_29_U19 : ex210 port map( A => count_5_port, B => add_29_n18, Y => N25);
   add_29_U18 : na210 port map( A => count_5_port, B => add_29_n18, Y => 
                           add_29_n17);
   add_29_U17 : ex210 port map( A => count_6_port, B => add_29_n17, Y => 
                           add_29_n16);
   add_29_U16 : ex210 port map( A => add_29_n8, B => add_29_n15, Y => N27);
   add_29_U15 : no210 port map( A => add_29_n15, B => add_29_n8, Y => 
                           add_29_n14);
   add_29_U14 : ex210 port map( A => count_8_port, B => add_29_n14, Y => N28);
   add_29_U13 : ex210 port map( A => count_9_port, B => add_29_n13, Y => N29);
   add_29_U12 : iv110 port map( A => count_15_port, Y => add_29_n12);
   add_29_U11 : iv110 port map( A => count_12_port, Y => add_29_n11);
   add_29_U10 : iv110 port map( A => count_11_port, Y => add_29_n10);
   add_29_U9 : iv110 port map( A => count_8_port, Y => add_29_n9);
   add_29_U8 : iv110 port map( A => count_7_port, Y => add_29_n8);
   add_29_U7 : iv110 port map( A => count_4_port, Y => add_29_n7);
   add_29_U6 : iv110 port map( A => count_3_port, Y => add_29_n6);
   add_29_U5 : iv110 port map( A => count_0_port, Y => N20);
   add_29_U4 : iv110 port map( A => add_29_n16, Y => N26);
   add_29_U3 : iv110 port map( A => add_29_n26, Y => N34);
   add_29_U2 : iv110 port map( A => add_29_n30, Y => N30);
   add_29_U1 : iv110 port map( A => add_29_n21, Y => N22);

end synthesised;



