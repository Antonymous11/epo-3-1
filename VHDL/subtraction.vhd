library IEEE;
use IEEE.std_logic_1164.ALL;

entity subtraction is
   port(clk   :in    std_logic;
        reset :in    std_logic;
        value1:in    std_logic_vector(7 downto 0);
        value2:in    std_logic_vector(7 downto 0);
        result:out   std_logic_vector(7 downto 0));
end subtraction;


