library IEEE;
use IEEE.std_logic_1164.ALL;

entity vga_sync is
port( clk, reset : in std_logic;
      hsync, vsync : out std_logic;
      row : out std_logic_vector(8 downto 0);
      column : out std_logic_vector(7 downto 0));
end vga_sync;

